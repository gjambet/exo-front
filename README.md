# Goal of the exercice

Goal of the exercise is to display the list of all players 

for each row : player's name, player's current team , Total number of goals he scored

this must looks good on both a phone and a computer

sorted by number of goals

# Ressources 

you can use the following apis :

* http://51.254.102.101:558/players
* http://51.254.102.101:558/teams
* http://51.254.102.101:558/matches

In cases thoses urls are down: 

-> you can run the server through it's docker iamge

* https://cloud.docker.com/repository/docker/trollheim/exo-front-backend
* docker pull trollheim/exo-front-backend:1.0-SNAPSHOT
* docker stop exo-front-backend
* docker rm  exo-front-backend
* docker create -p558:8080 --name exo-front-backend trollheim/exo-front-backend:1.0-SNAPSHOT
* docker start exo-front-backend


--> if you're more a java guy

* git clone https://gjambet@bitbucket.org/gjambet/exo-front-backend.git
* mvn clean package spring-boot:run

# Expectations : 
* unless told otherwise, you can use all ressources you feel like.
* we want to assess how you would do in an ideal world where you would act as principal devlopper of the team.
* we will assume the quality of the result is the best you can acheive.
* deadline is (within reasons) not an important constraint


# if you do miss your favourites teams & players
you can send a pr to : https://gjambet@bitbucket.org/gjambet/exo-front-backend





